=== Wallet One Payment Shopkeeper3 ===
Contributors: h-elena
Version: 3.0.0
Tags: Wallet One, Modx, buy, payment, payment for commerce, wallet one integration, Shopkeeper
Requires at least: 2.4.2
Tested up to: 2.5.2
Stable tag: 2.5.2

Tested extras Shopkeeper3 up to: 3.2.5

License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.en.html

The Wallet One module is a payment system for CMS Modx extras Shoopkeeper3. He it allows to pay for your orders on the site.

== Description ==
If you have an online store on CMS Modx, then you need a extras payments for orders made. This will help you extras the payment system Wallet One. With our system you will be able to significantly expand the method of receiving payment. This will lead to an increase in the number of customers to your store.

== Installation ==
1. Register on the site http://www.walletone.com
2. Download the Wallet One in the Extras -> Installer and install it.
3. Go to the settings page extra: Extras -> Payments extra Wallet One.
4. Fill in all required fields.
5. In page with a successful payment to place a snippet  [[!WalletOnePayment? &type=payment]]
6. Create a page with the result of the payment and put back a snippet [[!WalletOnePayment? &type=res_payment]]
Detailed instructions can be found on the website https://www.walletone.com/ru/merchant/modules/modx-shopkeeper3/

== Screenshots ==
1. w1_001.png
2. w1_002.png
3. w1_003.png
4. w1_004.png

== Changelog ==
= 1.0.1 =
* Added the handler when returning after outstanding orders

= 1.0.2 =
* Fixed bug with request payment system

= 2.0.0 =
* Add new settings and new universals classes

= 2.0.1 =
* Fixed bug with checkin signature in codding string windows-1251

= 3.0.0 =
* Fix - fixed bug with anser from calback payment system

== Frequently Asked Questions ==
No recent asked questions 

== Upgrade Notice ==
No recent asked updates
